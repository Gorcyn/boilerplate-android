# Tests

## Unit test

Run the unit tests :

    ❯ ./gradlew testMockDebugUnitTest --tests "com.gorcyn.boilerplate.*"

## Android instrumented tests

Run the test suite :

    ❯ ./gradlew app:connectedMockDebugAndroidTest -Pandroid.testInstrumentationRunnerArguments.class=com.gorcyn.boilerplate.tests.TestSuite
