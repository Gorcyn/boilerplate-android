package com.gorcyn.boilerplate.data.source

import retrofit2.http.GET
import retrofit2.http.Query

import io.reactivex.Observable

import com.gorcyn.boilerplate.data.entity.Contact

data class AuthResponse(val token: String)
data class FetchContactListResponse(val contacts: List<Contact>)

interface Service {

    @GET("c5bp3")
    fun auth(@Query("email") email: String, @Query("password") password: String): Observable<AuthResponse>

    @GET("c5bp3")
    fun fetchContactList(@Query("token") token: String): Observable<FetchContactListResponse>
}
