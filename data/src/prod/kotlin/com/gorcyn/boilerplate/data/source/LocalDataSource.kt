package com.gorcyn.boilerplate.data.source

import timber.log.Timber

import io.reactivex.Observable

import com.gorcyn.boilerplate.data.AppDatabase
import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.data.entity.User
import com.gorcyn.boilerplate.data.source.exception.UserNotFoundException
import com.gorcyn.boilerplate.data.source.operation.AuthOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactListOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveUserOperation

open class LocalDataSource(
        private val timber: Timber.Tree,
        private val database: AppDatabase
): DataSource() {

    override fun auth(email: String, password: String): Observable<AuthOperation> {

        return Observable.just<AuthOperation>(AuthOperation.Started)
            .onErrorReturn { AuthOperation.Error(it) }
            .map {
                this.database.userDao().findByEmailAndPassword(email, password)?.let {
                    AuthOperation.Success
                }
                AuthOperation.Error(UserNotFoundException())
            }
    }

    override fun saveUser(user: User): Observable<SaveUserOperation> {
        return Observable.just<SaveUserOperation>(SaveUserOperation.Started)
            .onErrorReturn { SaveUserOperation.Error(it) }
            .map { this.database.userDao().insertAll(user) }
            .map { SaveUserOperation.Success }
    }

    override fun fetchContactList(): Observable<FetchContactListOperation> {
        return Observable.just<FetchContactListOperation>(FetchContactListOperation.Started)
            .onErrorReturn { FetchContactListOperation.Error(it) }
            .map { this.database.contactDao().getAll() }
            .map { FetchContactListOperation.Success(it) }
    }

    override fun fetchContact(id: Long): Observable<FetchContactOperation> {
        return Observable.just<FetchContactOperation>(FetchContactOperation.Started)
            .onErrorReturn { FetchContactOperation.Error(it) }
            .map {
                this.database.contactDao().findById(id).let {
                    FetchContactOperation.Success(it!!)
                }
                FetchContactOperation.Error(UserNotFoundException())
            }
    }

    override fun saveContact(contact: Contact): Observable<SaveContactOperation> {
        return Observable.just<SaveContactOperation>(SaveContactOperation.Started)
            .onErrorReturn { SaveContactOperation.Error(it) }
            .map { this.database.contactDao().insertAll(contact) }
            .map { SaveContactOperation.Success }
    }
}
