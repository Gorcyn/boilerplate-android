package com.gorcyn.boilerplate.data.source.exception

class UserNotFoundException(message: String? = null): Exception(message)
