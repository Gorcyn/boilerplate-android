package com.gorcyn.boilerplate.data.source

import io.reactivex.Observable
import io.reactivex.rxkotlin.plusAssign

import timber.log.Timber

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.data.entity.User
import com.gorcyn.boilerplate.data.source.operation.AuthOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactListOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveUserOperation

open class Repository(
        val timber: Timber.Tree,
        private val localDataSource: DataSource,
        private val remoteDataSource: DataSource
): DataSource() {

    override fun auth(email: String, password: String): Observable<AuthOperation> {
        // We always try to auth form the remote
        return this.remoteDataSource.auth(email, password)
    }

    override fun saveUser(user: User): Observable<SaveUserOperation> {
        // Not available on remote
        return this.localDataSource.saveUser(user)
    }

    override fun fetchContactList(): Observable<FetchContactListOperation> {

        return this.remoteDataSource.fetchContactList()
            .doOnNext {
                this.timber.d(it.toString())
                when (it) {
                    is FetchContactListOperation.Success -> {
                        it.contactList.forEach { contact ->
                            disposables += this.localDataSource.saveContact(contact)
                                .subscribe({}, {
                                    timber.d(it, "Could not save contact $contact")
                                })
                        }
                    }
                }
            }
    }

    override fun fetchContact(id: Long): Observable<FetchContactOperation> {
        return this.remoteDataSource.fetchContact(id)
            .doOnNext {
                when (it) {
                    is FetchContactOperation.Success -> {
                        this.localDataSource.saveContact(it.contact)
                    }
                }
            }
    }

    override fun saveContact(contact: Contact): Observable<SaveContactOperation> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
