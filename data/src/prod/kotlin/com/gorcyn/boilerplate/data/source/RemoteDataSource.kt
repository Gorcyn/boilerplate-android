package com.gorcyn.boilerplate.data.source

import java.security.InvalidParameterException

import android.content.SharedPreferences

import io.reactivex.Observable

import timber.log.Timber

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.data.entity.User
import com.gorcyn.boilerplate.data.source.operation.AuthOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactListOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveUserOperation
import com.gorcyn.boilerplate.extensions.getToken
import com.gorcyn.boilerplate.extensions.putToken

open class RemoteDataSource(
        private val timber: Timber.Tree,
        private val service: Service,
        private val preferences: SharedPreferences
): DataSource() {

    override fun auth(email: String, password: String): Observable<AuthOperation> {
        timber.i("RemoteDataSource.auth($email, $password)")
        return service.auth(email, password)
            .doOnNext { preferences.putToken(it.token) }
            .map<AuthOperation> { AuthOperation.Success }
            .onErrorReturn { AuthOperation.Error(it) }
    }

    override fun saveUser(user: User): Observable<SaveUserOperation> {
        throw UnsupportedOperationException("Operation is not available!!!")
    }

    override fun fetchContactList(): Observable<FetchContactListOperation> {
        val token = preferences.getToken() ?: throw InvalidParameterException("Token must not be null")
        return service.fetchContactList(token)
            .map<FetchContactListOperation> { FetchContactListOperation.Success(it.contacts) }
            .onErrorReturn { FetchContactListOperation.Error(it) }
    }

    override fun fetchContact(id: Long): Observable<FetchContactOperation> {
        val token = preferences.getToken() ?: throw InvalidParameterException("Token must not be null")
        return service.fetchContactList(token)
            .map<FetchContactOperation> { FetchContactOperation.Success(it.contacts.first { it.id == id })}
            .onErrorReturn { FetchContactOperation.Error(it) }
    }

    override fun saveContact(contact: Contact): Observable<SaveContactOperation> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
