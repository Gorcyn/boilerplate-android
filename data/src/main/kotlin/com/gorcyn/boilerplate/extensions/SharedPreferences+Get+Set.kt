@file:Suppress("unused")

package com.gorcyn.boilerplate.extensions

import android.content.SharedPreferences

fun SharedPreferences.getToken(): String? {
    return this.getString("token", null)
}

fun SharedPreferences.putToken(token: String) {
    this.edit().putString("token", token).apply()
}
