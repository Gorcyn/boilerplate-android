package com.gorcyn.boilerplate.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable

import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "user")
data class User(
        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: Long,

        @ColumnInfo(name = "email")
        var email: String,

        @ColumnInfo(name = "name")
        var name: String,

        @ColumnInfo(name = "password")
        var password: String
): Parcelable
