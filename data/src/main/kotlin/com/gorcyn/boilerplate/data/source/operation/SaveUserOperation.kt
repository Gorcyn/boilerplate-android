package com.gorcyn.boilerplate.data.source.operation

sealed class SaveUserOperation {
    object Started: SaveUserOperation()
    object Success: SaveUserOperation()
    data class Error(val t: Throwable): SaveUserOperation()
}