package com.gorcyn.boilerplate.data.source.operation

sealed class AuthOperation {
    object Started: AuthOperation()
    object Success: AuthOperation()
    data class Error(val t: Throwable): AuthOperation()
}
