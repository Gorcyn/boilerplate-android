package com.gorcyn.boilerplate.data.source.operation

import com.gorcyn.boilerplate.data.entity.Contact

sealed class FetchContactListOperation {
    object Started: FetchContactListOperation()
    data class Success(val contactList: List<Contact>): FetchContactListOperation()
    data class Error(val t: Throwable): FetchContactListOperation()
}
