package com.gorcyn.boilerplate.data.source

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.data.entity.User
import com.gorcyn.boilerplate.data.source.operation.AuthOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactListOperation
import com.gorcyn.boilerplate.data.source.operation.SaveContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveUserOperation

abstract class DataSource {

    val disposables = CompositeDisposable()

    abstract fun auth(email: String, password: String): Observable<AuthOperation>
    abstract fun saveUser(user: User): Observable<SaveUserOperation>
    abstract fun fetchContactList(): Observable<FetchContactListOperation>
    abstract fun fetchContact(id: Long): Observable<FetchContactOperation>
    abstract fun saveContact(contact: Contact): Observable<SaveContactOperation>
}
