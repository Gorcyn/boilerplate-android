package com.gorcyn.boilerplate.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.OnConflictStrategy

import com.gorcyn.boilerplate.data.entity.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    fun getAll(): List<User>

    @Query("SELECT * FROM user WHERE id = :id")
    fun findById(id: Long): User?

    @Query("SELECT * FROM user WHERE email = :email AND password = :password")
    fun findByEmailAndPassword(email: String, password: String): User?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg user: User)

    @Delete
    fun delete(user: User)
}
