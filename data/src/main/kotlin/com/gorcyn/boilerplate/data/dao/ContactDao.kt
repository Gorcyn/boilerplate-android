package com.gorcyn.boilerplate.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.OnConflictStrategy

import com.gorcyn.boilerplate.data.entity.Contact

@Dao
interface ContactDao {

    @Query("SELECT * FROM contact")
    fun getAll(): List<Contact>

    @Query("SELECT * FROM contact WHERE id = :id")
    fun findById(id: Long): Contact?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg contact: Contact)

    @Delete
    fun delete(contact: Contact)
}
