package com.gorcyn.boilerplate.data.entity

import java.util.Date

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable

import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "contact")
data class Contact(
        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: Long,

        @ColumnInfo(name = "email")
        var email: String,

        @ColumnInfo(name = "name")
        var name: String,

        @ColumnInfo(name = "request_date")
        var requestDate: Date,

        @ColumnInfo(name = "validation_date")
        var validationDate: Date?,

        @ColumnInfo(name = "requested")
        var request: Boolean
): Parcelable
