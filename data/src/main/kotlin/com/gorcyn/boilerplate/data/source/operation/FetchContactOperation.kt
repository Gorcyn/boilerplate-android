package com.gorcyn.boilerplate.data.source.operation

import com.gorcyn.boilerplate.data.entity.Contact

sealed class FetchContactOperation {
    object Started: FetchContactOperation()
    data class Success(val contact: Contact): FetchContactOperation()
    data class Error(val t: Throwable): FetchContactOperation()
}
