package com.gorcyn.boilerplate.data.source.operation

sealed class SaveContactOperation {
    object Started: SaveContactOperation()
    object Success: SaveContactOperation()
    data class Error(val t: Throwable): SaveContactOperation()
}