package com.gorcyn.boilerplate.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

import com.gorcyn.boilerplate.data.dao.ContactDao
import com.gorcyn.boilerplate.data.dao.UserDao
import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.data.entity.User

@Database(entities = [(Contact::class), (User::class)], version = 1)
@TypeConverters(Converter::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun contactDao(): ContactDao

    abstract fun userDao(): UserDao
}
