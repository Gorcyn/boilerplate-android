package com.gorcyn.boilerplate.data.source

import java.util.Date

import io.reactivex.Observable

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.data.entity.User
import com.gorcyn.boilerplate.data.source.operation.AuthOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactListOperation
import com.gorcyn.boilerplate.data.source.operation.FetchContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveContactOperation
import com.gorcyn.boilerplate.data.source.operation.SaveUserOperation

open class FakeDataSource : DataSource() {

    companion object {
        const val EMAIL = "gorcyn@gmail.com"
        const val WRONG_EMAIL = "deckard@gmail.com"
        const val PASSWORD = "deckard"

        val CONTACT_LIST = listOf(
            Contact(1, "john@doe.com", "John", Date(), null, false),
            Contact(2, "jane@doe.com", "Jane", Date(), null, false),
            Contact(3, "jack@doe.com", "Jack", Date(), null, false),
            Contact(4, "jill@doe.com", "Jill", Date(), null, false)
        )
    }

    override fun auth(email: String, password: String) : Observable<AuthOperation> {

        return Observable.create {
            it.onNext(AuthOperation.Started)

            // Wait a second
            Thread.sleep(500)

            if (EMAIL == email && PASSWORD == password) {
                it.onNext(AuthOperation.Success)
            } else {
                it.onNext(AuthOperation.Error(Exception("Wrong credentials")))
            }
        }
    }

    override fun saveUser(user: User): Observable<SaveUserOperation> {
        return Observable.create {
            it.onNext(SaveUserOperation.Started)

            // Wait a second
            Thread.sleep(500)

            it.onNext(SaveUserOperation.Success)
        }
    }

    override fun fetchContactList(): Observable<FetchContactListOperation> {
        return Observable.create {
            it.onNext(FetchContactListOperation.Started)

            // Wait a second
            Thread.sleep(500)

            it.onNext(FetchContactListOperation.Success(CONTACT_LIST))
        }
    }

    override fun fetchContact(id: Long): Observable<FetchContactOperation> {
        return Observable.create {
            it.onNext(FetchContactOperation.Started)

            // Wait a second
            Thread.sleep(500)

            it.onNext(FetchContactOperation.Success(CONTACT_LIST.first { it.id == id }))
        }
    }

    override fun saveContact(contact: Contact): Observable<SaveContactOperation> {
        return Observable.create {
            it.onNext(SaveContactOperation.Started)

            // Wait a second
            Thread.sleep(500)

            it.onNext(SaveContactOperation.Success)
        }
    }
}
