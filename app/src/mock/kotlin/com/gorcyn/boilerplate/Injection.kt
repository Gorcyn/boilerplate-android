package com.gorcyn.boilerplate

import android.arch.persistence.room.Room
import android.content.Context

import timber.log.Timber

import org.kodein.di.Kodein.Builder
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

import com.gorcyn.boilerplate.data.AppDatabase
import com.gorcyn.boilerplate.data.source.DataSource
import com.gorcyn.boilerplate.data.source.FakeDataSource

fun Builder.provideDataSource() {
    bind<DataSource>("repository") with singleton { FakeDataSource() }
}

fun Builder.provideDataBase(context: Context) {
    bind<AppDatabase>() with singleton {
        Room.databaseBuilder(context, AppDatabase::class.java, "boilerplate").build()
    }
}

fun Builder.provideTimberTree() {
    bind<Timber.Tree>() with singleton { Timber.DebugTree() }
}
