package com.gorcyn.boilerplate.util

import android.util.Log

import timber.log.Timber

import com.crashlytics.android.Crashlytics

class CrashlyticsTree: Timber.Tree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        when (priority) {
            Log.VERBOSE, Log.DEBUG -> return
        }
        Crashlytics.log(priority, tag, message)
        t?.let {
            when (priority) {
                Log.ERROR -> Crashlytics.logException(t)
            }
        }
    }
}
