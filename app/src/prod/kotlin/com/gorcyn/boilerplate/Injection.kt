package com.gorcyn.boilerplate

import android.arch.persistence.room.Room
import android.content.Context

import timber.log.Timber

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

import com.google.gson.GsonBuilder

import org.kodein.di.Kodein.Builder
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

import com.gorcyn.boilerplate.data.AppDatabase
import com.gorcyn.boilerplate.data.source.DataSource
import com.gorcyn.boilerplate.data.source.Repository
import com.gorcyn.boilerplate.data.source.Service
import com.gorcyn.boilerplate.data.source.LocalDataSource
import com.gorcyn.boilerplate.data.source.RemoteDataSource
import com.gorcyn.boilerplate.util.CrashlyticsTree

fun Builder.provideDataSource() {

    bind<Service>() with singleton {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd")
            .create()

        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        Retrofit.Builder()
            .baseUrl("https://api.myjson.com/bins/")
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build().create(Service::class.java)
    }
    bind<DataSource>("local") with singleton {
        LocalDataSource(instance(), instance())
    }
    bind<DataSource>("remote") with singleton {
        RemoteDataSource(instance(), instance(), instance())
    }
    bind<DataSource>("repository") with singleton {
        Repository(instance(), instance("local"), instance("remote"))
    }
}

fun Builder.provideDataBase(context: Context) {
    bind<AppDatabase>() with singleton {
        Room.databaseBuilder(context, AppDatabase::class.java, "boilerplate").build()
    }
}

fun Builder.provideTimberTree() {
    bind<Timber.Tree>() with singleton { Timber.DebugTree() }
    // TODO: Put this back
    // bind<Timber.Tree>() with singleton { CrashlyticsTree(context) }
}
