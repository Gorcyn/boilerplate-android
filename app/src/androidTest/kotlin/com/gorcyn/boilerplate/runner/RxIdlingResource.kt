package com.gorcyn.boilerplate.runner

import java.util.concurrent.locks.ReentrantReadWriteLock

import android.support.test.espresso.IdlingResource
import android.support.test.espresso.IdlingResource.ResourceCallback

import io.reactivex.functions.Function

class RxIdlingResource: IdlingResource, Function<Runnable, Runnable> {

    companion object {
        var IDLING_STATE_LOCK = ReentrantReadWriteLock()
    }

    private var taskCount: Int = 0
    private var transitionCallback: ResourceCallback? = null

    override fun getName(): String {
        return javaClass.simpleName
    }

    override fun isIdleNow(): Boolean {
        return this.taskCount == 0
    }

    override fun registerIdleTransitionCallback(callback: ResourceCallback?) {
        IDLING_STATE_LOCK.writeLock().lock()
        this.transitionCallback = callback
        IDLING_STATE_LOCK.writeLock().unlock()
    }

    override fun apply(t: Runnable): Runnable {
        return Runnable {
            IDLING_STATE_LOCK.writeLock().lock()
            taskCount++
            IDLING_STATE_LOCK.writeLock().unlock()

            try {
                t.run()
            } finally {
                IDLING_STATE_LOCK.writeLock().lock()

                try {
                    taskCount--
                    if (taskCount == 0) {
                        transitionCallback?.onTransitionToIdle()
                    }
                } finally {
                    IDLING_STATE_LOCK.writeLock().unlock()
                }
            }
        }
    }
}
