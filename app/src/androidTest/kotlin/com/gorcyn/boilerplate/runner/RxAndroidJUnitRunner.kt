package com.gorcyn.boilerplate.runner

import android.os.Bundle
import android.support.test.espresso.Espresso
import android.support.test.runner.AndroidJUnitRunner

import io.reactivex.plugins.RxJavaPlugins

class RxAndroidJUnitRunner: AndroidJUnitRunner() {

    override fun onCreate(arguments: Bundle?) {
        super.onCreate(arguments)
        val rxIdlingResource = RxIdlingResource()
        Espresso.registerIdlingResources(rxIdlingResource)
        RxJavaPlugins.setScheduleHandler(rxIdlingResource)
    }
}
