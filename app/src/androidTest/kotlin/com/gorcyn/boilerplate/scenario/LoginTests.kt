package com.gorcyn.boilerplate.scenario

import org.junit.Test

import com.gorcyn.boilerplate.robot.contactDetail
import com.gorcyn.boilerplate.robot.contactList
import com.gorcyn.boilerplate.robot.login
import com.gorcyn.boilerplate.tests.UITest

class LoginTests: UITest() {

    @Test
    fun loginAsGorcyn() {
        login {
            iEnterEmail("gorcyn@gmail.com")
            iEnterPassword("deckard")
            iLogin()
        }
        contactList {
            iSeeAContact("Jane")
            iSelectAContact("Jane")
        }
        contactDetail {
            iSeeAContact("jane@doe.com")
        }
    }

    @Test
    fun failLoginAsJohn() {
        login {
            iEnterEmail("john@doe.com")
            iEnterPassword("deckard")
            iLogin()
            iSeeWrongCredentials()
        }
    }

    @Test
    fun cannotLoginWithInvalidEmail() {
        login {
            iEnterEmail("gorcyn")
            iSeeEmailIsInvalid()
        }
    }

    @Test
    fun cannotLoginWithInvalidPassword() {
        login {
            iEnterPassword("gorcyn")
            iSeePasswordIsInvalid()
        }
    }
}
