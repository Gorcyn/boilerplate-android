@file:Suppress("unused")

package com.gorcyn.boilerplate.extensions

import java.lang.reflect.Modifier

import org.hamcrest.Matchers.not

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.NoMatchingRootException
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions.click as vClick
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.doesNotExist
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.hasErrorText
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.isEnabled
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText

import com.gorcyn.boilerplate.R

fun Int.isView(): Boolean {
    return R.id::class.java.declaredFields.any { Modifier.isStatic(it.modifiers) && it.get(R.id()) as Int == this }
}

private fun Int.get(): ViewInteraction {
    if (this.isView()) {
        return onView(withId(this))
    }
    return onView(withText(this))
}

fun Int.canSee(): Int {
    this.get().check(matches(isDisplayed()))
    return this
}

fun Int.cannotSee(): Int {
    try {
        this.get().check(matches(not(isDisplayed())))
    } catch (e: NoMatchingRootException) {
        this.get().check(doesNotExist())
    }
    return this
}

fun Int.click(): Int {
    this.get().perform(vClick())
    return this
}

fun Int.isDisabled(): Int {
    this.get().check(matches(not(isEnabled())))
    return this
}

fun Int.type(text: String, releaseKeyboard: Boolean = false): Int {
    this.get().perform(typeText(text))
    if (releaseKeyboard) {
        this.get().perform(closeSoftKeyboard())
    }
    return this
}

fun Int.hasError(text: String): Int {
    this.get().check(matches(hasErrorText(text)))
    return this
}
