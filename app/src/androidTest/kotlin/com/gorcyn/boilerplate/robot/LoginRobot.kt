package com.gorcyn.boilerplate.robot

import com.gorcyn.boilerplate.R
import com.gorcyn.boilerplate.extensions.canSee
import com.gorcyn.boilerplate.extensions.click
import com.gorcyn.boilerplate.extensions.hasError
import com.gorcyn.boilerplate.extensions.type

fun login(func: LoginRobot.() -> Unit) {
    LoginRobot().apply { func() }
}

class LoginRobot {

    fun iEnterEmail(email: String) {
        R.id.loginEmailEditText.type(email)
    }

    fun iEnterPassword(password: String) {
        R.id.loginPasswordEditText.type(password)
    }

    fun iLogin() {
        R.id.loginLoginButton.click()
    }

    fun iSeeWrongCredentials() {
        "Wrong credentials".canSee()
    }

    fun iSeeEmailIsInvalid() {
        R.id.loginEmailEditText.hasError("Invalid")
    }

    fun iSeePasswordIsInvalid() {
        R.id.loginEmailEditText.hasError("Invalid")
    }
}
