package com.gorcyn.boilerplate.robot

import com.gorcyn.boilerplate.extensions.canSee

fun contactDetail(func: ContactDetailRobot.() -> Unit) {
    ContactDetailRobot().apply { func() }
}

class ContactDetailRobot {

    fun iSeeAContact(contactEmail: String) {
        contactEmail.canSee()
    }
}
