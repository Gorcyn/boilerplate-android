package com.gorcyn.boilerplate.robot

import com.gorcyn.boilerplate.extensions.canSee
import com.gorcyn.boilerplate.extensions.click

fun contactList(func: ContactListRobot.() -> Unit) {
    ContactListRobot().apply { func() }
}

class ContactListRobot {

    fun iSeeAContact(contactEmail: String) {
        contactEmail.canSee()
    }

    fun iSelectAContact(contactEmail: String) {
        contactEmail.click()
    }
}
