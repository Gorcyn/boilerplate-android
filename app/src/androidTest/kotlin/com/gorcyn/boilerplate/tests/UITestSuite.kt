package com.gorcyn.boilerplate.tests

import org.junit.runner.RunWith
import org.junit.runners.Suite

import com.gorcyn.boilerplate.scenario.LoginTests

@RunWith(Suite::class)

@Suite.SuiteClasses(
        LoginTests::class
)
class UITestSuite
