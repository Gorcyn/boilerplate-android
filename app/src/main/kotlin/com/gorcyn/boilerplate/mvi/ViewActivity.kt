package com.gorcyn.boilerplate.mvi

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity

import io.reactivex.disposables.CompositeDisposable

import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

import com.gorcyn.boilerplate.state.StateSaverDelegate

abstract class ViewActivity<VS, in V: BaseContract.View<VS>> : AppCompatActivity(), KodeinAware, BaseContract.View<VS> {

    override val kodein by closestKodein()

    val disposables = CompositeDisposable()
    private val stateSaver by lazy { StateSaverDelegate<AppCompatActivity>(this) }

    abstract val presenter: BaseContract.Presenter<VS, V>
    abstract val renderer: BaseContract.Renderer<VS, V>

    @SuppressLint("MissingSuperCall")
    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        stateSaver.onCreate(savedInstanceState)

        @Suppress("UNCHECKED_CAST")
        this.renderer.attachTo(this as V)
        this.presenter.attachView(this)
    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        stateSaver.onSaveInstanceState(outState)
    }

    @SuppressLint("MissingSuperCall")
    @CallSuper
    override fun onDestroy() {
        this.presenter.detachView()
        this.renderer.detachFrom()
        super.onDestroy()
    }

    override fun render(viewState: VS) {
        this.viewState = viewState
        this.renderer.render(viewState)
    }
}
