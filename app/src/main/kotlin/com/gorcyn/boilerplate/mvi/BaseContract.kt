package com.gorcyn.boilerplate.mvi

import com.gorcyn.boilerplate.state.StateRenderer

interface BaseContract {
    interface View<VS>: StateRenderer<VS> {
        var viewState: VS
    }
    interface Presenter<VS, in V: View<VS>> {
        fun attachView(view: V)
        fun detachView()
    }
    interface Renderer<VS, in V: View<VS>>: StateRenderer<VS> {
        fun attachTo(view: V)
        fun detachFrom()
    }
}
