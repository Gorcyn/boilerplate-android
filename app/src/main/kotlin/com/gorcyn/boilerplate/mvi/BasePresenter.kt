package com.gorcyn.boilerplate.mvi

import io.reactivex.disposables.CompositeDisposable

abstract class BasePresenter<VS, V: BaseContract.View<VS>>: BaseContract.Presenter<VS, V> {

    val disposables = CompositeDisposable()

    protected lateinit var view: V

    override fun attachView(view: V) {
        this.detachView()
        this.view = view
        this.bindIntents()
    }

    override fun detachView() {
        this.disposables.clear()
    }

    protected abstract fun bindIntents()
}
