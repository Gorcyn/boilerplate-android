package com.gorcyn.boilerplate.mvi

import io.reactivex.disposables.CompositeDisposable

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay

abstract class BaseRenderer<VS, V: BaseContract.View<VS>>: BaseContract.Renderer<VS, V> {

    val disposables = CompositeDisposable()
    val state: Relay<VS> = PublishRelay.create<VS>().toSerialized()

    protected lateinit var view: V

    override fun attachTo(view: V) {
        this.detachFrom()
        this.view = view
        this.bindUpdates()
    }

    override fun detachFrom() {
        this.disposables.clear()
    }

    override fun render(viewState: VS) {
        this.state.accept(viewState)
    }

    protected abstract fun bindUpdates()
}
