@file:Suppress("unused")

package com.gorcyn.boilerplate.extensions

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

fun Fragment.hideToolbar() {
    (activity as? AppCompatActivity)?.supportActionBar?.hide()
}

fun Fragment.showToolbar() {
    (activity as? AppCompatActivity)?.supportActionBar?.show()
}
