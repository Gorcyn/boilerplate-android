@file:Suppress("unused")

package com.gorcyn.boilerplate.extensions

import android.support.design.widget.BaseTransientBottomBar.LENGTH_SHORT
import android.support.design.widget.BaseTransientBottomBar.LENGTH_LONG
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.View

fun Fragment.makeSnack(text: CharSequence, duration: Int): Snackbar? {
    view?.let {
        return Snackbar.make(it, text, duration)
    }
    return null
}
fun Fragment.makeSnack(text: Int, duration: Int): Snackbar? {
    view?.let {
        return Snackbar.make(it, text, duration)
    }
    return null
}

fun Fragment.snack(text: CharSequence, duration: Int = LENGTH_SHORT) {
    makeSnack(text, duration)?.show()
}
fun Fragment.snack(text: Int, duration: Int = LENGTH_SHORT) {
    makeSnack(text, duration)?.show()
}
fun Fragment.snack(text: String, duration: Int = LENGTH_LONG, action: String, onAction: (View) -> Unit) {
    val snack = makeSnack(text, duration)
    snack?.setAction(action, onAction)?.show()
}
fun Fragment.snack(text: Int, duration: Int = LENGTH_LONG, action: Int, onAction: (View) -> Unit) {
    val snack = makeSnack(text, duration)
    snack?.setAction(action, onAction)?.show()
}
fun Fragment.snack(text: String, duration: Int = LENGTH_LONG, action: Int, onAction: (View) -> Unit) {
    val snack = makeSnack(text, duration)
    snack?.setAction(action, onAction)?.show()
}
fun Fragment.snack(text: Int, duration: Int = LENGTH_LONG, action: String, onAction: (View) -> Unit) {
    val snack = makeSnack(text, duration)
    snack?.setAction(action, onAction)?.show()
}
