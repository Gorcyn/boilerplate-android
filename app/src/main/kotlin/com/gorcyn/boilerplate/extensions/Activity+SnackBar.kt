@file:Suppress("unused")

package com.gorcyn.boilerplate.extensions

import android.app.Activity
import android.support.design.widget.BaseTransientBottomBar.LENGTH_LONG
import android.support.design.widget.BaseTransientBottomBar.LENGTH_SHORT
import android.support.design.widget.Snackbar
import android.view.View
import android.view.ViewGroup

fun Activity.makeSnack(text: CharSequence, duration: Int): Snackbar {
    return Snackbar.make(
            findViewById<ViewGroup>(android.R.id.content).getChildAt(0),
            text,
            duration
    )
}
fun Activity.makeSnack(text: Int, duration: Int): Snackbar {
    return Snackbar.make(
            findViewById<ViewGroup>(android.R.id.content).getChildAt(0),
        text,
        duration
    )
}

fun Activity.snack(text: CharSequence, duration: Int = LENGTH_SHORT) {
    makeSnack(text, duration).show()
}
fun Activity.snack(text: Int, duration: Int = LENGTH_SHORT) {
    makeSnack(text, duration).show()
}
fun Activity.snack(text: String, duration: Int = LENGTH_LONG, action: String, onAction: (View) -> Unit) {
    val snack = makeSnack(text, duration)
    snack.setAction(action, onAction).show()
}
fun Activity.snack(text: Int, duration: Int = LENGTH_LONG, action: Int, onAction: (View) -> Unit) {
    val snack = makeSnack(text, duration)
    snack.setAction(action, onAction).show()
}
fun Activity.snack(text: String, duration: Int = LENGTH_LONG, action: Int, onAction: (View) -> Unit) {
    val snack = makeSnack(text, duration)
    snack.setAction(action, onAction).show()
}
fun Activity.snack(text: Int, duration: Int = LENGTH_LONG, action: String, onAction: (View) -> Unit) {
    val snack = makeSnack(text, duration)
    snack.setAction(action, onAction).show()
}
