@file:Suppress("unused")

package com.gorcyn.boilerplate.extensions

import android.widget.TextView

fun TextView.setError(error: Int) {
    this.error = context.getString(error)
}
