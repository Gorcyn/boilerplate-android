package com.gorcyn.boilerplate.ui.contactList

import timber.log.Timber

import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign

import com.gorcyn.boilerplate.data.source.DataSource
import com.gorcyn.boilerplate.data.source.operation.FetchContactListOperation
import com.gorcyn.boilerplate.extensions.androidSchedulers
import com.gorcyn.boilerplate.mvi.BasePresenter
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change.LoadingAsked
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change.LoadingStarted
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change.ListChanged
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change.FetchingError

class ContactListPresenter(
        private val timber: Timber.Tree,
        private val dataSource: DataSource
): BasePresenter<ViewState, Contract.View>(), Contract.Presenter {

    override fun bindIntents() {
        timber.i("'%s' attached to '%s'", this.javaClass.simpleName, view.javaClass.simpleName)

        this.disposables += view.fetchIntent
            .map<Change> { LoadingAsked }
            .onErrorReturn(handleUnknownError)
            .map { view.viewState.reduce(it) }
            .doOnNext { timber.i(it.toString()) }
            .doOnNext { view.render(it) }
            .androidSchedulers()
            .subscribe({
                disposables += fetchContactList()
            }, {
                val state = view.viewState.reduce(FetchingError(it))
                view.render(state)
            })
    }

    private fun fetchContactList(): Disposable {
        return dataSource.fetchContactList()
                .map { operation -> handle(operation) }
                .onErrorReturn(handleUnknownError)
                .map { view.viewState.reduce(it) }
                .doOnNext { timber.i(it.toString()) }
                .androidSchedulers()
                .subscribe({
                    view.render(it)
                }, {
                    val state = view.viewState.reduce(FetchingError(it))
                    view.render(state)
                })
    }

    companion object {
        private val handleUnknownError: (Throwable) -> Change = ::FetchingError

        private fun handle(operation: FetchContactListOperation): Change = when (operation) {
            is FetchContactListOperation.Started -> LoadingStarted
            is FetchContactListOperation.Success -> ListChanged(operation.contactList)
            is FetchContactListOperation.Error -> FetchingError(operation.t)
        }
    }
}
