package com.gorcyn.boilerplate.ui.contactList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import de.hdodenhof.circleimageview.CircleImageView

import io.reactivex.disposables.Disposable

import com.jakewharton.rxbinding2.view.clicks

import com.gorcyn.boilerplate.R
import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.data.gravatarUrl
import com.gorcyn.boilerplate.util.GlideApp

import kotlinx.android.synthetic.main.item_contact.view.contactListContactAvatar
import kotlinx.android.synthetic.main.item_contact.view.contactListContactName

class ContactListAdapter(
        private val onItemClickListener: (contact: Contact) -> Unit
): RecyclerView.Adapter<ContactListAdapter.ViewHolder>() {

    var items: List<Contact> = emptyList()

    private var disposable: Disposable? = null

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return ViewHolder(view)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        disposable = null
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.name.text = items[position].name

        disposable = viewHolder.itemView.clicks().subscribe({ onItemClickListener(items[position]) }, {})

        GlideApp.with(viewHolder.itemView.context)
            .load(items[position].gravatarUrl)
            .placeholder(R.drawable.ic_icon)
            .into(viewHolder.avatar)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.contactListContactName
        val avatar: CircleImageView = itemView.contactListContactAvatar
    }
}
