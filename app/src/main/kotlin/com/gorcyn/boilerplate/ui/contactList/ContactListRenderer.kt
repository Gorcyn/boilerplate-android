package com.gorcyn.boilerplate.ui.contactList

import timber.log.Timber

import io.reactivex.rxkotlin.plusAssign

import com.gorcyn.boilerplate.extensions.androidSchedulers
import com.gorcyn.boilerplate.mvi.BaseRenderer

class ContactListRenderer(
        private val timber: Timber.Tree
): BaseRenderer<ViewState, Contract.View>(), Contract.Renderer {

    override fun bindUpdates() {
        timber.i("'%s' attached to '%s'", view.javaClass.simpleName, this.javaClass.simpleName)

        // Update isLoading
        disposables += state
            .map { it.isLoading }
            .androidSchedulers()
            .subscribe({
                view.isLoading = it
            }, {
                view.error = it
            })

        // Update contactList
        disposables += state
            .map { it.contactList.sortedBy { it.name } }
            .androidSchedulers()
            .subscribe({
                view.contactList = it
            }, {
                view.error = it
            })

        // Update error
        disposables += state
            .androidSchedulers()
            .subscribe({
                view.error = it.error
            }, {
                view.error = it
            })
    }
}
