package com.gorcyn.boilerplate.ui.contactDetail

import timber.log.Timber

import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign

import com.gorcyn.boilerplate.data.source.DataSource
import com.gorcyn.boilerplate.data.source.operation.FetchContactOperation
import com.gorcyn.boilerplate.extensions.androidSchedulers
import com.gorcyn.boilerplate.mvi.BasePresenter
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change.LoadingAsked
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change.LoadingStarted
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change.ContactChanged
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change.FetchingError

class ContactDetailPresenter(
        private val timber: Timber.Tree,
        private val dataSource: DataSource
): BasePresenter<ViewState, Contract.View>(), Contract.Presenter {

    override fun bindIntents() {
        timber.i("'%s' attached to '%s'", this.javaClass.simpleName, view.javaClass.simpleName)

        this.disposables += view.fetchIntent
            .map<Change> { LoadingAsked(it) }
            .onErrorReturn(handleUnknownError)
            .map { view.viewState.reduce(it) }
            .doOnNext { timber.i(it.toString()) }
            .doOnNext { view.render(it) }
            .filter { it.id != null }
            .androidSchedulers()
                .subscribe({
                    it.id?.let {
                        disposables += fetchContact(it)
                    }
                }, {
                    val state = view.viewState.reduce(FetchingError(it))
                    view.render(state)
                })

    }

    private fun fetchContact(id: Long): Disposable {
        return dataSource.fetchContact(id)
                .map { operation -> handle(operation) }
                .onErrorReturn(handleUnknownError)
                .map { view.viewState.reduce(it) }
                .doOnNext { timber.i(it.toString()) }
                .androidSchedulers()
                .subscribe({
                    view.render(it)
                }, {
                    val state = view.viewState.reduce(FetchingError(it))
                    view.render(state)
                })
    }

    companion object {
        private val handleUnknownError: (Throwable) -> Change = ::FetchingError

        private fun handle(operation: FetchContactOperation): Change = when (operation) {
            is FetchContactOperation.Started -> LoadingStarted
            is FetchContactOperation.Success -> ContactChanged(operation.contact)
            is FetchContactOperation.Error -> FetchingError(operation.t)
        }
    }
}
