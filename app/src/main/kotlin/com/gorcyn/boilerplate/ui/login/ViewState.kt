package com.gorcyn.boilerplate.ui.login

import android.os.Parcelable
import android.support.v4.util.PatternsCompat

import com.gorcyn.boilerplate.ui.login.ViewState.Change.EmailChange
import com.gorcyn.boilerplate.ui.login.ViewState.Change.PasswordChange
import com.gorcyn.boilerplate.ui.login.ViewState.Change.LoginAsked
import com.gorcyn.boilerplate.ui.login.ViewState.Change.LoginStarted
import com.gorcyn.boilerplate.ui.login.ViewState.Change.LoginError
import com.gorcyn.boilerplate.ui.login.ViewState.Change.LoginSuccess

import kotlinx.android.parcel.Parcelize

@Parcelize
data class ViewState(
        val email: String = "",
        val password: String = "",
        val isLoading: Boolean = false,
        val loginSuccess: Boolean = false,
        val error: Throwable? = null,
        val invalidEmail: Boolean = true,
        val invalidPassword: Boolean = true
): Parcelable {

    val isValid: Boolean
        get() = !invalidEmail && !invalidPassword

    sealed class Change {
        data class EmailChange(val email: String): Change()
        data class PasswordChange(val password: String): Change()
        object LoginAsked: Change()
        object LoginStarted: Change()
        object LoginSuccess: Change()
        data class LoginError(val error: Throwable): Change()
    }

    fun reduce(change: Change): ViewState = when (change) {
        is EmailChange -> copy(email = change.email, invalidEmail = !validateEmail(change.email), error = null)
        is PasswordChange -> copy(password = change.password, invalidPassword = !validatePassword(change.password), error = null)
        is LoginAsked -> copy(isLoading = false, loginSuccess = false, error = null)
        is LoginStarted -> copy(isLoading = true, error = null)
        is LoginSuccess -> copy(isLoading = false, loginSuccess = true, error = null)
        is LoginError -> copy(isLoading = false, loginSuccess = false, error = change.error)
    }

    private fun validateEmail(email: String) = !email.isEmpty() && PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()
    private fun validatePassword(password: String): Boolean = !password.isEmpty() && password.length > 3
}
