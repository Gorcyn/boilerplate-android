package com.gorcyn.boilerplate.ui.login

import org.kodein.di.Kodein
import org.kodein.di.android.AndroidComponentsWeakScope
import org.kodein.di.generic.scoped
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

import com.jakewharton.rxrelay2.Relay

import com.gorcyn.boilerplate.mvi.BaseContract

interface Contract: BaseContract {

    interface View: BaseContract.View<ViewState> {
        override var viewState: ViewState

        //region Intents
        val emailIntent: Relay<String>
        val passwordIntent: Relay<String>
        val loginIntent: Relay<Unit>
        //endregion

        //region UI
        var invalidEmail: Boolean
        var invalidPassword: Boolean
        var canLogin: Boolean
        var isLoading: Boolean
        var loginSuccess: Boolean
        var error: Throwable?
        //endregion
    }
    interface Presenter: BaseContract.Presenter<ViewState, View>
    interface Renderer: BaseContract.Renderer<ViewState, View>
}

var loginModule = Kodein.Module("Login") {
    bind<Contract.Presenter>() with scoped(AndroidComponentsWeakScope).singleton { LoginPresenter(instance(), instance("repository")) }
    bind<Contract.Renderer>() with scoped(AndroidComponentsWeakScope).singleton { LoginRenderer(instance()) }
}
