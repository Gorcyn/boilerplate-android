package com.gorcyn.boilerplate.ui.contactDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import org.kodein.di.generic.instance

import com.jakewharton.rxrelay2.Relay
import com.jakewharton.rxrelay2.PublishRelay

import timber.log.Timber

import com.evernote.android.state.State

import com.gorcyn.boilerplate.R
import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.data.gravatarUrl
import com.gorcyn.boilerplate.extensions.showToolbar
import com.gorcyn.boilerplate.extensions.snack
import com.gorcyn.boilerplate.mvi.ViewFragment
import com.gorcyn.boilerplate.util.GlideApp

import kotlinx.android.synthetic.main.fragment_contact_detail.contactDetailContactAvatar
import kotlinx.android.synthetic.main.fragment_contact_detail.contactDetailContactEmail
import kotlinx.android.synthetic.main.fragment_contact_detail.contactDetailContactName

class ContactDetailFragment: ViewFragment<ViewState, Contract.View>(), Contract.View {

    companion object {
        const val TAG = "ContactDetailFragment"
        const val CONTACT_ID = "id"

        fun newInstance(id: Long): ContactDetailFragment {
            val bundle = Bundle()
            bundle.putLong(CONTACT_ID, id)
            val fragment = ContactDetailFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    val timber: Timber.Tree by instance()
    override val renderer: Contract.Renderer by instance()
    override val presenter: Contract.Presenter by instance()

    //region State
    @State
    override var viewState = ViewState()

    override val fetchIntent: Relay<Long> = PublishRelay.create()
    //endregion

    //region UI
    override var isLoading: Boolean = false
    override var contact: Contact? = null
        set(value) {
            if (value != null) {
                timber.i(value.gravatarUrl)
                GlideApp.with(this)
                    .load(value.gravatarUrl)
                    .placeholder(R.drawable.ic_icon)
                    .into(contactDetailContactAvatar)
                contactDetailContactName.text = value.name
                contactDetailContactEmail.text = value.email
            } else {
                contactDetailContactAvatar.setImageResource(android.R.color.transparent)
                contactDetailContactName.text = null
                contactDetailContactEmail.text = null
            }
        }
    override var error: Throwable? = null
        set(value) {
            if (value != null) {
                snack(value.localizedMessage, action = "Ok", onAction = {
                    timber.i("Dismissed action")
                })
            }
        }
    //endregion

    //region Lifecycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showToolbar()

        arguments?.let {
            val contactId: Long = it.getLong(CONTACT_ID)
            if (0L != contactId) {
                fetchIntent.accept(contactId)
            } else {
                TODO("This should never appear but...")
            }
        }
    }
    //endregion
}
