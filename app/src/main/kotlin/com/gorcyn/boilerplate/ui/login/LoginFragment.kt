package com.gorcyn.boilerplate.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import timber.log.Timber

import io.reactivex.rxkotlin.addTo

import org.kodein.di.generic.instance

import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.textChanges

import com.jakewharton.rxrelay2.Relay
import com.jakewharton.rxrelay2.PublishRelay

import com.gorcyn.boilerplate.R
import com.gorcyn.boilerplate.extensions.hideToolbar
import com.gorcyn.boilerplate.extensions.replaceFragmentInActivity
import com.gorcyn.boilerplate.extensions.setError
import com.gorcyn.boilerplate.extensions.snack
import com.gorcyn.boilerplate.mvi.ViewFragment
import com.gorcyn.boilerplate.ui.contactList.ContactListFragment

import kotlinx.android.synthetic.main.fragment_login.loginEmailEditText
import kotlinx.android.synthetic.main.fragment_login.loginPasswordEditText
import kotlinx.android.synthetic.main.fragment_login.loginLoginButton

class LoginFragment: ViewFragment<ViewState, Contract.View>(), Contract.View {

    companion object {
        fun newInstance() = LoginFragment()
    }

    val timber: Timber.Tree by instance()
    override val renderer: Contract.Renderer by instance()
    override val presenter: Contract.Presenter by instance()

    //region State
    override var viewState = ViewState()

    override val emailIntent: Relay<String> = PublishRelay.create()
    override val passwordIntent: Relay<String> = PublishRelay.create()
    override val loginIntent: Relay<Unit> = PublishRelay.create()
    //endregion

    //region UI
    override var invalidEmail: Boolean = false
        set(value) {
            if (value) loginEmailEditText.setError(R.string.email_invalid)
            else loginEmailEditText.error = null
        }
    override var invalidPassword: Boolean = false
        set(value) {
            if (value) loginPasswordEditText.setError(R.string.password_invalid)
            else loginPasswordEditText.error = null
        }
    override var canLogin: Boolean = false
        set(value) {
            loginLoginButton.isEnabled = value
        }
    override var isLoading: Boolean = false
        set(value) {
            loginEmailEditText.isEnabled = !value
            loginPasswordEditText.isEnabled = !value
        }
    override var loginSuccess: Boolean = false
        set(value) {
            if (value) {
                replaceFragmentInActivity(ContactListFragment.newInstance(), R.id.container, ContactListFragment.TAG)
            }
        }
    override var error: Throwable? = null
        set(value) {
            if (value != null) {
                snack(value.localizedMessage, action = "Ok", onAction = {
                    timber.i("Dismissed action")
                })
            }
        }
    //endregion

    //region Lifecycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        hideToolbar()

        loginEmailEditText.textChanges()
            .doOnNext { timber.i(it.toString()) }
            .forEach { emailIntent.accept(it.toString()) }
            .addTo(this.disposables)
        loginPasswordEditText.textChanges()
            .forEach { passwordIntent.accept(it.toString()) }
            .addTo(this.disposables)
        loginLoginButton.clicks()
            .forEach { loginIntent.accept(Unit) }
            .addTo(this.disposables)
    }
    //endregion
}
