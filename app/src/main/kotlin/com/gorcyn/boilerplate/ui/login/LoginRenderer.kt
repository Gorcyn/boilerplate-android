package com.gorcyn.boilerplate.ui.login

import io.reactivex.rxkotlin.plusAssign

import timber.log.Timber

import com.gorcyn.boilerplate.mvi.BaseRenderer
import com.gorcyn.boilerplate.extensions.androidSchedulers

class LoginRenderer(
        private val timber: Timber.Tree
): BaseRenderer<ViewState, Contract.View>(), Contract.Renderer {

    override fun bindUpdates() {
        timber.i("'%s' attached to '%s'", view.javaClass.simpleName, this.javaClass.simpleName)

        // Update invalidEmail
        disposables += state
            .map { it.invalidEmail }
            .androidSchedulers()
            .subscribe({
                view.invalidEmail = it
            }, {
                view.error = it
            })

        // Update invalidPassword
        disposables += state
            .map { it.invalidPassword }
            .androidSchedulers()
            .subscribe({
                view.invalidPassword = it
            }, {
                view.error = it
            })

        // Update isValid
        disposables += state
            .map { it.isValid }
            .androidSchedulers()
            .subscribe({
                view.canLogin = it
            }, {
                view.error = it
            })

        // Update loading
        disposables += state
            .map { it.isLoading }
            .distinctUntilChanged()
            .androidSchedulers()
            .subscribe({
                view.isLoading = it
            }, {
                view.error = it
            })

        // Update loginSuccess
        disposables += state
            .map { it.loginSuccess }
            .distinctUntilChanged()
            .androidSchedulers()
            .subscribe({
                view.loginSuccess = it
            }, {
                view.error = it
            })

        // Update error
        disposables += state
            .distinctUntilChanged()
            .androidSchedulers()
            .subscribe({
                view.error = it.error
            }, {
                view.error = it
            })
    }
}
