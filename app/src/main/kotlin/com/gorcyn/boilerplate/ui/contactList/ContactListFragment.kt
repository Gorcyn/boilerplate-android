package com.gorcyn.boilerplate.ui.contactList

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import io.reactivex.rxkotlin.addTo

import timber.log.Timber

import org.kodein.di.generic.instance

import com.jakewharton.rxrelay2.Relay
import com.jakewharton.rxrelay2.PublishRelay

import com.evernote.android.state.State

import com.gorcyn.boilerplate.R
import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.extensions.replaceFragmentInActivity
import com.gorcyn.boilerplate.extensions.showToolbar
import com.gorcyn.boilerplate.extensions.snack
import com.gorcyn.boilerplate.mvi.ViewFragment
import com.gorcyn.boilerplate.ui.contactDetail.ContactDetailFragment
import com.jakewharton.rxbinding2.support.v4.widget.refreshes

import kotlinx.android.synthetic.main.fragment_contact_list.contactListProgressBar
import kotlinx.android.synthetic.main.fragment_contact_list.contactListRecyclerView

class ContactListFragment: ViewFragment<ViewState, Contract.View>(), Contract.View {

    companion object {
        const val TAG = "ContactListFragment"
        fun newInstance() = ContactListFragment()
    }

    val timber: Timber.Tree by instance()
    override val renderer: Contract.Renderer by instance()
    override val presenter: Contract.Presenter by instance()

    private val adapter: ContactListAdapter = ContactListAdapter {
        replaceFragmentInActivity(ContactDetailFragment.newInstance(it.id), R.id.container, ContactDetailFragment.TAG)
    }

    //region State
    @State
    override var viewState = ViewState()

    override val fetchIntent: Relay<Unit> = PublishRelay.create()
    //endregion

    //region UI
    override var isLoading: Boolean = false
    set(value) {
        contactListProgressBar.isRefreshing = value
    }
    override var contactList: List<Contact> = emptyList()
    set(value) {
        adapter.items = value
        adapter.notifyDataSetChanged()
    }
    override var error: Throwable? = null
    set(value) {
        if (value != null) {
            snack(value.localizedMessage, action = "Ok", onAction = {
                timber.i("Dismissed action")
            })
        }
    }
    //endregion

    //region Lifecycle
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        showToolbar()

        contactListRecyclerView.layoutManager = LinearLayoutManager(view.context)
        contactListRecyclerView.adapter = adapter

        contactListProgressBar.refreshes()
                .forEach { fetchIntent.accept(Unit) }
                .addTo(this.disposables)
    }

    override fun onStart() {
        super.onStart()
        contactListProgressBar.isRefreshing = true
        fetchIntent.accept(Unit)
    }
    //endregion
}
