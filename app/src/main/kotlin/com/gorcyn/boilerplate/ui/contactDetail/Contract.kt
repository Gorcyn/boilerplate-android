package com.gorcyn.boilerplate.ui.contactDetail

import org.kodein.di.Kodein
import org.kodein.di.android.AndroidComponentsWeakScope
import org.kodein.di.generic.scoped
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

import com.jakewharton.rxrelay2.Relay

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.mvi.BaseContract

interface Contract: BaseContract {

    interface View: BaseContract.View<ViewState> {
        //region State
        override var viewState: ViewState
        val fetchIntent: Relay<Long>
        //endregion

        //region UI
        var isLoading: Boolean
        var contact: Contact?
        var error: Throwable?
        //endregion
    }
    interface Presenter: BaseContract.Presenter<ViewState, View>
    interface Renderer: BaseContract.Renderer<ViewState, View>
}

var contactDetailModule = Kodein.Module("ContactDetail") {
    bind<Contract.Presenter>() with scoped(AndroidComponentsWeakScope).singleton { ContactDetailPresenter(instance(), instance("repository")) }
    bind<Contract.Renderer>() with scoped(AndroidComponentsWeakScope).singleton { ContactDetailRenderer(instance()) }
}
