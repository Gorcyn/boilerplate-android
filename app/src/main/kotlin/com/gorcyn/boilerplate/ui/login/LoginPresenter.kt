package com.gorcyn.boilerplate.ui.login

import timber.log.Timber

import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign

import com.gorcyn.boilerplate.data.source.DataSource
import com.gorcyn.boilerplate.data.source.operation.AuthOperation
import com.gorcyn.boilerplate.extensions.androidSchedulers
import com.gorcyn.boilerplate.mvi.BasePresenter
import com.gorcyn.boilerplate.ui.login.ViewState.Change
import com.gorcyn.boilerplate.ui.login.ViewState.Change.EmailChange
import com.gorcyn.boilerplate.ui.login.ViewState.Change.PasswordChange
import com.gorcyn.boilerplate.ui.login.ViewState.Change.LoginAsked
import com.gorcyn.boilerplate.ui.login.ViewState.Change.LoginStarted
import com.gorcyn.boilerplate.ui.login.ViewState.Change.LoginError
import com.gorcyn.boilerplate.ui.login.ViewState.Change.LoginSuccess

class LoginPresenter(
        private val timber: Timber.Tree,
        private val dataSource: DataSource
): BasePresenter<ViewState, Contract.View>(), Contract.Presenter {

    override fun bindIntents() {
        timber.i("'%s' attached to '%s'", this.javaClass.simpleName, view.javaClass.simpleName)

        disposables += view.emailIntent
            .distinctUntilChanged()
            .map<Change>(::EmailChange)
            .onErrorReturn(handleUnknownError)
            .map { view.viewState.reduce(it) }
            .doOnNext { timber.i(it.toString()) }
            .subscribe({
                view.render(it)
            }, {
                val state = view.viewState.reduce(LoginError(it))
                view.render(state)
            })

        disposables += view.passwordIntent
            .distinctUntilChanged()
            .map<Change>(::PasswordChange)
            .onErrorReturn(handleUnknownError)
            .map { view.viewState.reduce(it) }
            .doOnNext { timber.i(it.toString()) }
            .subscribe({
                view.render(it)
            }, {
                val state = view.viewState.reduce(LoginError(it))
                view.render(state)
            })

        disposables += view.loginIntent
            .map<Change> { LoginAsked }
            .onErrorReturn(handleUnknownError)
            .map { view.viewState.reduce(it) }
            .doOnNext { timber.i(it.toString()) }
            .doOnNext { view.render(it) }
            .filter { it.isValid }
            .androidSchedulers()
                .subscribe({
                    disposables += auth(it.email, it.password)
                }, {
                    val state = view.viewState.reduce(LoginError(it))
                    view.render(state)
                })
    }

    private fun auth(email: String, password: String): Disposable {
        return dataSource.auth(email, password)
                .map { operation -> handle(operation) }
                .onErrorReturn(handleUnknownError)
                .map { view.viewState.reduce(it) }
                .doOnNext { timber.i(it.toString()) }
                .androidSchedulers()
                .subscribe({
                    view.render(it)
                }, {
                    val state = view.viewState.reduce(LoginError(it))
                    view.render(state)
                })
    }

    companion object {
        private val handleUnknownError: (Throwable) -> Change = ::LoginError

        private fun handle(operation: AuthOperation): Change = when (operation) {
            is AuthOperation.Started -> LoginStarted
            is AuthOperation.Success -> LoginSuccess
            is AuthOperation.Error -> LoginError(operation.t)
        }
    }
}
