package com.gorcyn.boilerplate.ui.contactList

import android.os.Parcelable

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change.LoadingAsked
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change.LoadingStarted
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change.ListChanged
import com.gorcyn.boilerplate.ui.contactList.ViewState.Change.FetchingError

import kotlinx.android.parcel.Parcelize

@Parcelize
data class ViewState(
        val isLoading: Boolean = false,
        val contactList: List<Contact> = emptyList(),
        val error: Throwable? = null
): Parcelable {

    sealed class Change {
        object LoadingAsked: Change()
        object LoadingStarted: Change()
        data class ListChanged(val contactList: List<Contact>): Change()
        data class FetchingError(val error: Throwable): Change()
    }

    fun reduce(change: Change): ViewState = when (change) {
        is LoadingAsked -> copy(contactList = emptyList(), error = null)
        is LoadingStarted -> copy(isLoading = true)
        is ListChanged -> copy(contactList = change.contactList)
        is FetchingError -> copy(error = change.error)
    }
}
