package com.gorcyn.boilerplate.ui.contactDetail

import android.os.Parcelable

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change.ContactChanged
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change.FetchingError
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change.LoadingAsked
import com.gorcyn.boilerplate.ui.contactDetail.ViewState.Change.LoadingStarted

import kotlinx.android.parcel.Parcelize

@Parcelize
data class ViewState(
        val isLoading: Boolean = false,
        val id: Long? = null,
        val contact: Contact? = null,
        val error: Throwable? = null
): Parcelable {

    sealed class Change {
        data class LoadingAsked(val id: Long): Change()
        object LoadingStarted: Change()
        data class ContactChanged(val contact: Contact): Change()
        data class FetchingError(val error: Throwable): Change()
    }

    fun reduce(change: Change): ViewState = when (change) {
        is LoadingAsked -> copy(id = change.id, contact = null, error = null)
        is LoadingStarted -> copy(isLoading = true)
        is ContactChanged -> copy(contact = change.contact)
        is FetchingError -> copy(error = change.error)
    }
}
