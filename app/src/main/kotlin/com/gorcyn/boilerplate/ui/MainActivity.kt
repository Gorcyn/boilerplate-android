package com.gorcyn.boilerplate.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity

import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

import com.gorcyn.boilerplate.R
import com.gorcyn.boilerplate.extensions.replaceFragmentInActivity
import com.gorcyn.boilerplate.ui.login.LoginFragment

import kotlinx.android.synthetic.main.activity_main.toolbar

class MainActivity: AppCompatActivity(), KodeinAware {

    override val kodein by closestKodein()

    //region Lifecycle
    @SuppressLint("MissingSuperCall")
    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        supportFragmentManager.findFragmentById(R.id.container) ?: LoginFragment.newInstance().also {
            replaceFragmentInActivity(it, R.id.container)
        }
    }
    //endregion
}
