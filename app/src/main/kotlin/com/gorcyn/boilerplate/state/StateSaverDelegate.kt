package com.gorcyn.boilerplate.state

import android.os.Bundle

import com.evernote.android.state.StateSaver

class StateSaverDelegate<T>(private val target: T) {

    fun onCreate(savedState: Bundle?) {
        StateSaver.restoreInstanceState(target, savedState)
    }

    fun onSaveInstanceState(outState: Bundle?) {
        if (outState != null) {
            StateSaver.saveInstanceState(target, outState)
        }
    }
}
