package com.gorcyn.boilerplate.state

interface StateRenderer<in VS> {
    fun render(viewState: VS)
}
