package com.gorcyn.boilerplate

import android.app.Application

import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.androidModule

import com.gorcyn.boilerplate.ui.login.loginModule
import com.gorcyn.boilerplate.ui.contactList.contactListModule
import com.gorcyn.boilerplate.ui.contactDetail.contactDetailModule

@Suppress("unused")
class BoilerplateApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidModule(this@BoilerplateApplication))

        provideTimberTree()
        provideDataBase(this@BoilerplateApplication)
        provideDataSource()

        import(loginModule)
        import(contactListModule)
        import(contactDetailModule)
    }
}
