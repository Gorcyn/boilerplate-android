package com.gorcyn.boilerplate.data

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.extensions.md5

val Contact.gravatarUrl: String
    get() = "http://www.gravatar.com/avatar/${this.email.md5()}?d=mm".toLowerCase()
