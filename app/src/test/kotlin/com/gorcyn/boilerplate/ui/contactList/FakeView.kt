package com.gorcyn.boilerplate.ui.contactList

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay

import com.gorcyn.boilerplate.data.entity.Contact
import com.gorcyn.boilerplate.ui.contactList.Contract.View

class FakeView: View {
    override fun render(viewState: ViewState) {
        this.viewState = viewState
    }

    override var viewState: ViewState = ViewState()

    override val fetchIntent: Relay<Unit> = PublishRelay.create()

    override var isLoading: Boolean = false
    override var contactList: List<Contact> = emptyList()
    override var error: Throwable? = null
}
