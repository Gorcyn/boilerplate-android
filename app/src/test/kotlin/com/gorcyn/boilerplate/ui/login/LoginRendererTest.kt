package com.gorcyn.boilerplate.ui.login

import org.junit.Before
import org.junit.Test

import timber.log.Timber

import org.mockito.MockitoAnnotations

import com.nhaarman.mockito_kotlin.mock

import com.winterbe.expekt.should

import com.gorcyn.boilerplate.tests.RxTest
import com.gorcyn.boilerplate.ui.login.Contract.View
import com.gorcyn.boilerplate.ui.login.Contract.Renderer
import com.gorcyn.boilerplate.ui.login.ViewState.Change.EmailChange
import com.gorcyn.boilerplate.ui.login.ViewState.Change.PasswordChange

class LoginRendererTest: RxTest() {

    private val timber: Timber.Tree = mock()
    private val view: View = FakeView()

    private lateinit var renderer: Renderer

    @Before
    fun beforeEachTest() {
        MockitoAnnotations.initMocks(this)
        renderer = LoginRenderer(timber)
        renderer.attachTo(view)
    }

    @Test
    fun validEmailChangesArePushedToView() {
        renderer.render(ViewState().reduce(EmailChange("gorcyn@gmail.com")))

        view.invalidEmail.should.equal(false)
    }

    @Test
    fun invalidEmailChangesArePushedToView() {
        renderer.render(ViewState().reduce(EmailChange("gorcyn")))

        view.invalidEmail.should.equal(true)
    }

    @Test
    fun validPasswordChangesArePushedToView() {
        renderer.render(ViewState().reduce(PasswordChange("deckard")))

        view.invalidPassword.should.equal(false)
    }

    @Test
    fun invalidPasswordChangesArePushedToView() {
        renderer.render(ViewState().reduce(PasswordChange("dec")))

        view.invalidPassword.should.equal(true)
    }
}
