package com.gorcyn.boilerplate.ui.login

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay

import com.gorcyn.boilerplate.ui.login.Contract.View

class FakeView: View {
    override fun render(viewState: ViewState) {
        this.viewState = viewState
    }

    override var viewState: ViewState = ViewState()

    override val emailIntent: Relay<String> = PublishRelay.create()
    override val passwordIntent: Relay<String> = PublishRelay.create()
    override val loginIntent: Relay<Unit> = PublishRelay.create()

    override var invalidEmail: Boolean = true
    override var invalidPassword: Boolean = true
    override var canLogin: Boolean = false
    override var isLoading: Boolean = false
    override var loginSuccess: Boolean = false
    override var error: Throwable? = null
}
