package com.gorcyn.boilerplate.ui.contactList

import org.junit.Before
import org.junit.Test

import timber.log.Timber

import org.mockito.MockitoAnnotations

import com.nhaarman.mockito_kotlin.mock

import com.winterbe.expekt.should

import com.gorcyn.boilerplate.tests.RxTest
import com.gorcyn.boilerplate.data.source.DataSource
import com.gorcyn.boilerplate.data.source.FakeDataSource
import com.gorcyn.boilerplate.ui.contactList.Contract.View
import com.gorcyn.boilerplate.ui.contactList.Contract.Presenter

class ContactListPresenterTest: RxTest() {

    private val timber: Timber.Tree = mock()
    private val view: View = FakeView()
    private val repository: DataSource = FakeDataSource()

    private lateinit var presenter: Presenter

    @Before
    fun beforeEachTest() {
        MockitoAnnotations.initMocks(this)
        presenter = ContactListPresenter(timber, repository)
        presenter.attachView(view)
    }

    @Test
    fun contactListChangesArePushedToViewState() {
        view.fetchIntent.accept(Unit)

        view.viewState.contactList.should.equal(FakeDataSource.CONTACT_LIST)
    }
}
