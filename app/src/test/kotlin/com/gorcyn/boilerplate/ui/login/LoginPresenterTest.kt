package com.gorcyn.boilerplate.ui.login

import org.junit.Before
import org.junit.Test

import timber.log.Timber

import org.mockito.MockitoAnnotations

import com.nhaarman.mockito_kotlin.mock

import com.winterbe.expekt.should

import com.gorcyn.boilerplate.tests.RxTest
import com.gorcyn.boilerplate.data.source.DataSource
import com.gorcyn.boilerplate.data.source.FakeDataSource
import com.gorcyn.boilerplate.ui.login.Contract.View
import com.gorcyn.boilerplate.ui.login.Contract.Presenter

class LoginPresenterTest: RxTest() {

    private val timber: Timber.Tree = mock()
    private val view: View = FakeView()
    private val repository: DataSource = FakeDataSource()

    private lateinit var presenter: Presenter

    @Before
    fun beforeEachTest() {
        MockitoAnnotations.initMocks(this)
        presenter = LoginPresenter(timber, repository)
        presenter.attachView(view)
    }

    @Test
    fun emailChangesArePushedToViewState() {
        view.emailIntent.accept("")
        view.emailIntent.accept(FakeDataSource.EMAIL)

        view.viewState.email.should.equal(FakeDataSource.EMAIL)
        view.viewState.invalidEmail.should.be.`false`
    }

    @Test
    fun passwordChangesArePushedToViewState() {
        view.passwordIntent.accept("")
        view.passwordIntent.accept(FakeDataSource.PASSWORD)

        view.viewState.password.should.equal(FakeDataSource.PASSWORD)
        view.viewState.invalidPassword.should.be.`false`
    }

    @Test
    fun loginSuccessIsPushedToViewState() {
        view.emailIntent.accept("")
        view.emailIntent.accept(FakeDataSource.EMAIL)

        view.passwordIntent.accept("")
        view.passwordIntent.accept(FakeDataSource.PASSWORD)

        view.loginIntent.accept(Unit)

        view.viewState.email.should.equal(FakeDataSource.EMAIL)
        view.viewState.invalidEmail.should.be.`false`
        view.viewState.password.should.equal(FakeDataSource.PASSWORD)
        view.viewState.invalidPassword.should.be.`false`
        view.viewState.isValid.should.be.`true`
        view.viewState.error.should.be.`null`
        view.viewState.isLoading.should.be.`false`
        view.viewState.loginSuccess.should.be.`true`
    }

    @Test
    fun loginErrorIsPushedToViewState() {
        view.emailIntent.accept("")
        view.emailIntent.accept(FakeDataSource.WRONG_EMAIL)

        view.passwordIntent.accept("")
        view.passwordIntent.accept(FakeDataSource.PASSWORD)

        view.loginIntent.accept(Unit)

        view.viewState.email.should.equal(FakeDataSource.WRONG_EMAIL)
        view.viewState.invalidEmail.should.be.`false`
        view.viewState.password.should.equal(FakeDataSource.PASSWORD)
        view.viewState.invalidPassword.should.be.`false`
        view.viewState.isValid.should.be.`true`
        view.viewState.error.should.be.not.`null`
        view.viewState.isLoading.should.be.`false`
        view.viewState.loginSuccess.should.be.`false`
    }
}
