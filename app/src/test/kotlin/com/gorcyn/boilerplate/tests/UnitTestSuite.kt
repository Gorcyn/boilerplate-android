package com.gorcyn.boilerplate.tests

import org.junit.runner.RunWith
import org.junit.runners.Suite

import com.gorcyn.boilerplate.ui.contactList.ContactListPresenterTest
import com.gorcyn.boilerplate.ui.login.LoginPresenterTest
import com.gorcyn.boilerplate.ui.login.LoginRendererTest

@RunWith(Suite::class)

@Suite.SuiteClasses(
        LoginPresenterTest::class,
        LoginRendererTest::class,
        ContactListPresenterTest::class
)
class UnitTestSuite
